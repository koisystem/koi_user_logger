<?php
namespace KoiSys\WPUserLog;
use KoiSys\WPUserLog\Logger\Logger;
use KoiSys\WPUserLog\Logger\Adapter\FileSystem;
use KoiSys\WPUserLog\Session;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WPUserLoggerPlugin
{

    public function boot()
    {
        $this->init_hooks();
    }

    private function init_hooks() {
        add_action( 'wp_login' , array($this, 'user_has_logged_in') );
    }

    public function user_has_logged_in($username) {

        $session = new Session($username);

        $lineParts = [];
        $lineParts[] = $session->getStartTime();
        $lineParts[] = $session->getUserLogin();
        $lineParts[] = $session->getUserRoles(); 
        $lineParts[] = $session->getUserIP();


        /**
         * Built adapters so that plugin can be easily extsneible in the future
         * Can create adapters for database, log services, http, slack, etc
         * 
         * Stored on a insecure location, would quickly secure it by changing the htaccess, by adding
         *     <Files logs.txt>
         *       Order allow,deny 
         *       Deny from all
         *      </Files>
         */
        $logger = new Logger( new FileSystem(__DIR__."/../logs/logs.txt") );
        $logger->log( implode(",",$lineParts) );
        
    }   
   

}
