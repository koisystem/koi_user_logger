<?php

/**
 * Class: Session
 *
 * Class to access client Session and data
 *
 * @package koi-user-logger
 */

namespace KoiSys\WPUserLog;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Session
{
    private $startTime  = null;
    private $user       = null;

    public function __construct($login) {
        $this->startTime    =  date("c");
        $this->user         =  get_user_by("login", $login);
    }

    public function getStartTime() {
        return $this->startTime;
    }

    public function getUserLogin() {
      
        return $this->user->data->user_login;
    }

    public function getUserRoles() {
        return implode("|", $this->user->roles);
    }

    public function getUserIP() {

        /**
         * Handle cloudflare ip forwarding
         */
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }

        $client  = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : '';
        $forward = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
        $remote  = $_SERVER['REMOTE_ADDR'];
     
        $ip = '';
        if( filter_var($client, FILTER_VALIDATE_IP) ) { 
            $ip = $client; 
        }
        elseif( filter_var($forward, FILTER_VALIDATE_IP) ) { 
            $ip = $forward; 
        }
        else { 
            $ip = $remote; 
        }

        return $ip;
    }
   
}