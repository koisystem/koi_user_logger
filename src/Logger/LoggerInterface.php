<?php

/**
 * Class: LoggerInterface
 *
 * Interface class for the logger
 *
 * @package koi-user-logger
 */

namespace KoiSys\WPUserLog\Logger;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

interface LoggerInterface
{
    public function log($message);
}