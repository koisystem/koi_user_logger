<?php

/**
 * Class: FileSystem
 *
 * Simple FileSystem logger adapter for this plugin
 *
 * @package koi-user-logger
 */

namespace KoiSys\WPUserLog\Logger\Adapter;
use KoiSys\WPUserLog\Logger\LoggerInterface;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class FileSystem implements LoggerInterface
{
    private $path;
    public function __construct($path) {
        $this->path = $path;
    }
    public function log($message) {
        file_put_contents($this->path, $message.PHP_EOL, FILE_APPEND);
    }
}