<?php

/**
 * Class: Logger
 *
 * Logger class for the plugin
 *
 * @package koi-user-logger
 */

namespace KoiSys\WPUserLog\Logger;
use KoiSys\WPUserLog\Logger\LoggerInterface;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Logger
{
    private $loggerAdapter = null;

    public function __construct(LoggerInterface $adapter) {
        $this->loggerAdapter    =   $adapter;
    }

    public function log($message) {
        $this->loggerAdapter->log($message);
    }
}