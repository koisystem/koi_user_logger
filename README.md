# KOI User Logger Wordpress Plugin

## This plugin was developed as an interview code, has not been reviewed for production usage.


### Permission 
In order to have logs writen on the file please make sure you have the correct permissions on the folder: 

  wp-content/plugins/koi_user_logger/logs/

### Production usage : Not recommended

Please add the following protection to your htaccess file:

<Files logs.txt>

    Order allow,deny 
    
    Deny from all
    
</Files>