<?php
/*
Plugin Name: User Authentication Logger
Description: Logs every user login on a log
Plugin URI:
Author: Jose da Silva
Author URI: 
Version: 1.0.0
*/


require dirname(__FILE__) . '/vendor/autoload.php';

use  KoiSys\WPUserLog\WPUserLoggerPlugin;
$gapi = new WPUserLoggerPlugin();
$gapi->boot();